---
title: "16S analysis"
author: "Anthoula Gaigneaux & Dominik Ternes"
date: "10/03/2020"
output: "html_document"
---

```{r global_options, include=F}
# library(vegan)
# library(microbiome)
# library(phyloseq)
library(scales)
library(DESeq2)
# library(RColorBrewer)
library(tidyverse)
library(fs)
# library(DEGreport)
# library(extrafont)
# library(ggkm)
# loadfonts(device = "win")

data_folder <- "data"
out_folder <- "out"

dir_create(file.path(out_folder, "01"), recurse = TRUE)
```

```{r Functions}
computeresults <- function(contrast, dds, pThreshold=0.05, fcThreshold=1, exprThreshold=0){
  message(contrast)
  # extract results and p.adjust
  res <- results(dds,
    contrast = contrast,
    alpha = pThreshold, tidy = TRUE) %>%
    mutate(signif = ifelse(!is.na(padj) & padj <= pThreshold & abs(log2FoldChange) >= log2(fcThreshold) & baseMean>=exprThreshold, TRUE, FALSE))
}
```

Differential expression between cancer and healthy samples. We have seen that collection site needs to take into account as batch

# Load data

```{r Load data}

res <- readxl::read_xlsx(file.path(data_folder, "01", "DE_cancer_healthy.xlsx"))
metdat <- read_csv(file.path(data_folder, "01", "SOCS_16S_availability.csv"))
load(file.path(data_folder, "01", "dds_20200312.rda"))
file1 <- readxl::read_xlsx(file.path(data_folder, "01", "file1_mod.xlsx"))

```

Table of samples:
```{r}
# table(sample_data(pseq)$Health.status)
```


## Aggregate data to Genus level

Taxa defined by sequence, but not annotated at species level. 
Hence group all sequence counts by Genus to facilitate interpretation and regroup counts.

```{r}
# pseq_agg <- tax_glom(pseq, taxrank = 'Genus') # rem also tested on family. quite similar
# taxa_names(pseq_agg) <- tax_table(pseq_agg)[,'Genus']
# pseq_agg
```


# PERMANOVA

on aggregated data
```{r}
# tmp <- phyloseq::distance(physeq = pseq_agg, method = "bray")
# adonis.test <- vegan::adonis(tmp ~ Health.status , data = file1)
# adonis.test
```
About 3% of variance explained bby Health status.


# PERMANOVA covariates

on aggregated data, using age and gender as covariate (we loose samples).

```{r}
# physeq = subset_samples(pseq_agg, !is.na(age) & !is.na(gender))
# tmp <- phyloseq::distance(physeq, method = "bray")
# adonis.test <- vegan::adonis(tmp ~ age + gender + Health.status , data = sample_data( physeq) %>% as_tibble())
# adonis.test
```
Gender not significant.

# Differential expression

## Materials and methods

Analyses were performed in R version 3.6 (ref), using microbiome related packages (phyloseq, microbiome) in Bioconductor framework (ref). Fastq files were processed to counts using dada2 (ref). According to quality plots, reads were left trimmed (at 10) and truncated at 280 (forward), 220(reverse) before dada2 process. Library sizes ranged from 50k to 200k reads, hence samples were not rarefied and differential expression was performed using DEseq2, which can account for library size differences. In particular, we used "poscounts" SizeFactor estimator that accounts for the many zeros present in such data. Data were annotated up to genus level, and aggregated to genus level for differitial expression analysis. Genuses were called differently expressed between cancer and healthy samples when the (FDR) adj.p-value <= 0.05, and average expression was higher than 5 (threshold based on overall distribution).

## Results on aggregated data


```{r}
# dds <- phyloseq_to_deseq2(pseq_agg, ~ Health.status) #variables in design formula cannot contain NA: age
# dds = estimateSizeFactors(dds, type='poscounts') # adapted for many 0
# dds = DESeq(dds) #check ok, actually automatically change for local if not adapted
resultsNames(dds)
```
Still replacing outliers and refitting for 1/3 genes (removing the collection site decreased number of genes affected)

expression distribution

```{r}
ggplot(as_data_frame(results(dds)), aes(baseMean+1)) + geom_histogram(binwidth=0.1) + geom_vline(xintercept = 2+1) + scale_x_log10()
```


Results (pThreshold=0.05, exprThreshold=2)
```{r}
pThreshold=0.05
exprThreshold=2

# res<- list("Cancer_vs_Healthy" = list("Health.status_cancer_vs_no_cancer")) %>% 
#   enframe("name", "contrast") %>% 
#   mutate(results=map(contrast, computeresults, dds, pThreshold, exprThreshold)) %>% 
#   select(-contrast) %>% unnest(results) %>% 
#   left_join(tax_table(pseq_agg) %>% as("matrix") %>% as_tibble( rownames="row") )
```


plotMA

```{r}
DESeq2::plotMA(res%>%
     select(baseMean, log2FoldChange, signif), 
  ylim = c(-2,2), main="Cancer_vs_Healthy")
```

Volcano plot:

```{r}
ggplot(res, aes(log2FoldChange, -log10(pvalue), col=signif)) +
  geom_point(alpha=0.5) +
  ylim(0,10) + xlim(-10,10)
```


top results:
```{r}
# res %>% filter(signif==TRUE) %>% 
#   arrange(pvalue) %>% 
#   #dplyr::select(-stat)
```

write full result table, with expression
```{r}
# res %>% 
#   #dplyr::select(-stat) %>% 
#   left_join(as_tibble(counts(dds, normalized=TRUE), rownames="row")) %>% 
#   writexl::write_xlsx(., "results/DE_cancer_healthy.xlsx")
```

# Graphs

## Expression distribution of significant taxa at Genus level.
```{r fig.height=5, fig.width=10}
CRCbac <- c("Clostridium_sensu_stricto_1", "Fusobacterium", "Gemella", "Parvimonas", "Peptostreptococcus", "Porphyromonas", "Prevotella", "Prevotella_7", "Prevotellaceae_Ga6A1_group", "Streptococcus")

p <- as_tibble(counts(dds, normalized=TRUE)[res$signif==TRUE & !is.na(res$signif),]+1, rownames="Genus") %>% 
  filter(Genus %in% CRCbac) %>% #CRC-associated bacteria
  pivot_longer(-Genus) %>% 
  left_join(file1, by= c("name" = "samples")) %>%  
  left_join(metdat) %>% 
  mutate(Health.status = ifelse(Health.status == "cancer", "Cancer", "Healthy")) %>%
  mutate(Health.status = fct_relevel(Health.status, "Healthy", "Cancer")) %>% 
  mutate(Genus = gsub("_sensu_stricto_1", "_ss1", Genus)) %>%
  mutate(Genus = gsub("_group", "", Genus)) %>% 
  ggplot(aes(Health.status, value, color = Health.status)) + 
  geom_violin(col = "grey") +
  geom_jitter(width = 0.2) + 
  scale_color_manual(values=c("steelblue", "orange")) +
  scale_y_log10(breaks = trans_breaks("log10", function(x) 10^x), 
                labels = trans_format("log10", scales::math_format(10^.x)))+
  theme_bw() + 
  facet_wrap(~Genus, ncol = 5) +
  theme(text = element_text(size = 14, family = "sans"), 
        legend.position = "top", 
        legend.box = "horizontal",
        axis.text = element_text(color = "black"),
        axis.title = element_text(size = "10"),
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        strip.text = element_text(face = "italic")) +
  labs(y = "baseMean", x = "", color = "Health status:") +
  guides(color = guide_legend(override.aes = list(size = 5)))

last_plot()
st_2 <- as_tibble(p$data)
ggsave(filename = file.path(out_folder, "01", "Signif_tumor.png"), p, height = 5, width = 10)
```

## Foldchange by Phylum

from [here](https://bioconductor.org/packages/devel/bioc/vignettes/phyloseq/inst/doc/phyloseq-mixture-models.html#deseq2-conversion-and-call)

```{r}
theme_set(theme_bw())
sigtabgen = res %>% filter(!is.na(row), signif==TRUE)
# Phylum order
x = tapply(sigtabgen$log2FoldChange, sigtabgen$Class, function(x) max(x))
x = sort(x, TRUE)
sigtabgen$Class = factor(as.character(sigtabgen$Class), levels=names(x))
# Genus order
x = tapply(sigtabgen$log2FoldChange, sigtabgen$Genus, function(x) max(x))
x = sort(x, TRUE)
sigtabgen$Genus = factor(as.character(sigtabgen$Genus), levels = names(x))
p <- ggplot(sigtabgen, aes(y = Phylum, x = log2FoldChange, fill = Class, size = padj)) + 
  geom_vline(xintercept = 0.0, color = "gray", size = 0.5) +
  geom_point(shape = 21, colour = "black") +
  scale_size_continuous(range = c(4, 7.5)) + 
  theme(text = element_text(size = 14, family = "sans"), 
        legend.box = "horizontal",
        axis.text = element_text(color = "black"),
        axis.title = element_text(size = "10")) +
  scale_fill_brewer(palette = "Set1") + 
  guides(fill = guide_legend(override.aes = list(size = 5))) +
  labs(size = "p-value")

last_plot()
ggsave(filename = file.path(out_folder, "01", "profileSignif_Phylum_class.png"), p, height = 3, width = 7, device = 'png')
```

## Profile for significant taxa

```{r}
mat <- log10(counts(dds, normalized=TRUE)[res$signif==TRUE & !is.na(res$signif) & res$log2FoldChange>0,]+1) #Only CRC-enriched
mat <- log10(counts(dds, normalized=TRUE)[res$signif==TRUE & !is.na(res$signif) & res$log2FoldChange<0,]+1) #Only CRC-depleted
mat <- log10(counts(dds, normalized=TRUE)[res$signif==TRUE & !is.na(res$signif),]+1) #all

mat <- mat[,order(dds$Health.status)] %>% as.data.frame() 

height <- 2+nrow(mat)*0.2
width <- 3+ncol(mat)*0.05

# optional
annotation_col <- file1 %>% 
  right_join(tibble(samples=colnames(mat))) %>% 
  select(samples, Health.status) %>% 
  as.data.frame() %>% 
  mutate("Health status" = ifelse(Health.status == "no_cancer", "Healthy", "Cancer"), Health.status = NULL) %>% 
  column_to_rownames("samples")

# ann_col <- DEGreport::degColors(annotation_col) 
ann_col <- list("Health status" = c(Healthy = "steelblue", Cancer = "orange"))

#Italicize row labels
newnames <- lapply(
  rownames(mat),
  function(x) bquote(italic(.(x))))

p <- pheatmap::pheatmap(mat, scale = "none", 
  cluster_cols = T, 
  clustering_distance_cols = "correlation",
  cluster_rows = T, 
  clustering_distance_rows = "correlation",
  border_color = NA,
  color = colorRampPalette(c("#000033", "#66CCFF"))(100), #phyloseq colors
  annotation_col = annotation_col, 
  show_colnames = FALSE, 
  annotation_colors = ann_col, 
  annotation_legend = T, 
  legend = T,
  legend_breaks = 1:6, 
  legend_labels = c("", "2", "", "4", "", "6"),
  show_rownames = TRUE,  
  fontsize_row = 13, 
  fontsize = 15,
  silent = TRUE,
  labels_row = as.expression(newnames)) 
p

# Assign red labels according to position
p$gtable$grobs[[4]]$gp=grid::gpar(col=c(rep("#000000", 17), "#FD2C0C", rep("#000000", 2), rep("#FD2C0C", 7), "#000000", rep("#FD2C0C", 2), rep("#000000", 3)), fontsize=12) # assuming that the xlabels are in the third grob


ggsave(filename = file.path(out_folder, "01", "profileSignif_Genus_col.png"), p, height = 7, width = 12, device = 'png')
grid::grid.newpage()
grid::grid.draw(p$gtable)
```

```{r How many Fuso+ cases}
as_tibble(counts(dds, normalized=TRUE)[res$signif==TRUE & !is.na(res$signif),]+1, rownames="Genus") %>%  
     filter(Genus == "Fusobacterium") %>% #CRC-associated bacteria
     pivot_longer(-Genus) %>% 
     left_join(file1, by= c("name" = "samples")) %>%  
     left_join(metdat) %>% 
     mutate(Health.status = ifelse(Health.status == "cancer", "Cancer", "Healthy")) %>%
     mutate(Health.status = fct_relevel(Health.status, "Healthy", "Cancer")) %>% 
     mutate(Genus = gsub("_sensu_stricto_1", "_ss1", Genus)) %>%
     mutate(Genus = gsub("_group", "", Genus)) %>% 
    filter(Health.status == "Cancer") %>% 
    mutate(test = ifelse(value == 1, "neg", "pos")) %>% 
    count(test)
```

