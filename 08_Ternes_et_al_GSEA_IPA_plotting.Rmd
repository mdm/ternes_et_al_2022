---
title: "GSEA IPA plotting"
author: "DT"
date: "12/9/2020"
output: "html_document"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(fs)
library(readxl)

data_folder <- "data"
out_folder <- "out"

dir_create(file.path(out_folder, "08"), recurse = TRUE)
```

Based on IPA analysis, cellular functions were extracted that are increased in Fuso vs. Non-Fuso

exported excel contains sorted activation score filtered for increased activation

# HuMiX

## Canonical pathways

```{r IPA visualization (TCGA)-1, fig.width=10, fig.height=5}

Extracted_pathways_TCGA <- c("IL-8 Signaling",
                             "Actin Nucleation by ARP-WASP Complex",
                             "p38 MAPK Signaling",
                             "Tec Kinase Signaling",
                             "Integrin Signaling",
                             "Colorectal Cancer Metastasis Signaling",
                             "Wnt/Ca+ pathway",
                             "LXR/RXR Activation",
                             "Th17 Activation Pathway",
                             "Aryl Hydrocarbon Receptor Signaling")

ipa_HuMiX <- read_excel(file.path(data_folder, "08", "HuMiX", "CRCHuMiX", "RNA-seq", "IPA", "Canonical_pathways.xls"), skip = 1) %>%
  dplyr::select(diseasefun = "Ingenuity Canonical Pathways", 
                zscore = "z-score", padj = "-log(p-value)", Molecules) %>%
  mutate(zscore = ifelse(is.na(zscore), 0, zscore)) %>% 
  mutate(molecules = lengths(strsplit(.$Molecules, ","))) 

ipa_HuMiX %>% mutate(diseasefun = fct_reorder(diseasefun, zscore)) %>%
    #top_n(10, zscore) %>% 
    filter(diseasefun %in% Extracted_pathways_TCGA) -> ipa_HuMiX_plot

ipa_HuMiX_plot %>% 
    ggplot(aes(x = zscore, y = diseasefun, size = molecules, color = padj)) +
    geom_point() + 
    xlab("Activation score (z-score)") +
    ylab("") +
    theme_classic() +
    scale_colour_gradient(low = "#FF0000", high = "#F6ECEB") +
    labs(size="# of molecules", colour="-log(p-value)") +
    theme(axis.text.y = element_text(size = 18),
          axis.text.x = element_text(size = 16),
          axis.title.x = element_text(size = 16)) 

ggsave(file.path(out_folder, "08", "IPA_HuMiX_dis_pat_plot_top.png"), last_plot(), device = "png", width = 10, height = 5)

```

## Disease or Function

```{r IPA visualization, fig.width=8, fig.height=5}

ipa <- read_excel(file.path(data_folder, "08", "HuMiX", "CRCHuMiX", "RNA-seq", "IPA", "Function_increased activation states_functions.xls"), skip = 1) %>% 
  dplyr::select(diseasefun = "Diseases or Functions Annotation", zscore = "Activation z-score", padj = "p-value", molecules = "# Molecules")

ipa %>% mutate(diseasefun = fct_reorder(diseasefun, zscore)) %>%
  top_n(10, zscore) %>% 
  ggplot(aes(x = zscore, y = diseasefun, size = molecules, color = padj)) +
  geom_point() + 
  xlab("Activation score (z-score)") +
  ylab("") +
  theme_classic() +
  scale_colour_gradient(high = "#80bfff",
                        low = "#00264d") +
  labs(size="Involved genes", colour="p-value") +
  theme(axis.text.y = element_text(size = 18),
        axis.text.x = element_text(size = 16),
  axis.title.x = element_text(size = 16)) 

ggsave(file.path(out_folder, "08", "IPA_dis_func_plot_top.png"), last_plot(), device = "png", width = 8, height = 5)

```

## GSEA (pathfindr - KEGG)

```{r Plot HuMiX RA_Output, fig.width=8, fig.height=5}

Extracted_pathways_HuMiX <- c("Cell cycle",
                        "Proteoglycans in cancer",
                        "Insulin signaling pathway",
                        "Transcriptional misregulation in cancer",
                        "ErbB signaling pathway",
                        "NF-kappa B signaling pathway",
                        "MAPK signaling pathway",
                        "Focal adhesion",
                        "HIF-1 signaling pathway",
                        "p53 signaling pathway",
                        "Natural killer cell mediated cytotoxicity",
                        "Colorectal cancer",
                        "Adherens junction",
                        "Pathogenic Escherichia coli infection",
                        "Salmonella infection",
                        "Wnt signaling pathway",
                        "Aryl Hydrocarbon Receptor Signaling")

HuMiX_RA_output <- read.csv(file.path(data_folder, "08", "HuMiX", "CRCHuMiX", "RNA-seq", "pathfindR", "HuMiX_RA_output_Kegg.csv")) %>% 
   filter(Term_Description %in% Extracted_pathways_HuMiX) 

HuMiX_RA_output %>% 
  mutate(Term_Description = fct_reorder(Term_Description, Fold_Enrichment)) %>% 
  mutate(n_up = lengths(strsplit(.$Up_regulated, ","))) %>% 
  mutate(n_down = lengths(strsplit(.$Down_regulated, ","))) %>% 
  mutate(ratio_updown = log(n_up/n_down)) %>% 
  mutate(n_both = n_up+n_down)  -> HuMiX_RA_output_plot 

HuMiX_RA_output_plot %>% 
  ggplot(aes(x = Fold_Enrichment, y = Term_Description, size = n_both, color = lowest_p)) +
  geom_point() + 
  xlab("Fold Enrichment") +
  ylab("") +
  theme_classic() +
  scale_colour_gradient(low = "#FF0000", high = "#F6ECEB") +
  labs(size="# genes", color = "p-value") +
  theme(axis.text.y = element_text(size = 16),
        axis.text.x = element_text(size = 14),
  axis.title.x = element_text(size = 14)) 

ggsave(file.path(out_folder, "08", "KEGG_HuMiX_RA_output_plot_selected.png"), last_plot(), device = "png", width = 8, height = 5)

```

# TCGA

## Canonical pathways

```{r IPA visualization (TCGA), fig.width=10, fig.height=5}

Extracted_pathways_TCGA <- c("IL-8 Signaling",
                             "Tumor Microenvironment Pathway",
                             "Colorectal Cancer Metastasis Signaling",
                             "Inhibition of Matrix Metalloproteases",
                             "RhoGDI Signaling",
                             "Integrin Signaling",
                             "PTEN Signaling",
                             "Th17 Activation Pathway",
                             "Aryl Hydrocarbon Receptor Signaling",
                             "Actin Cytoskeleton Signaling")

ipa_TCGA <- read_excel(file.path(data_folder, "08", "IPA", "Full_TCGA_fuso_load_c3_high_vs.c0_no_075.xls"), skip = 1) %>%
  dplyr::select(diseasefun = "Ingenuity Canonical Pathways", 
                zscore = "z-score", padj = "-log(p-value)", Molecules) %>%
  mutate(zscore = ifelse(is.na(zscore), 0, zscore)) %>% 
  mutate(molecules = lengths(strsplit(.$Molecules, ","))) 

ipa_TCGA %>% mutate(diseasefun = fct_reorder(diseasefun, zscore)) %>%
    filter(diseasefun %in% Extracted_pathways_TCGA) %>%
    ggplot(aes(x = zscore, y = diseasefun, size = molecules, color = padj)) +
    geom_point() + 
    xlab("Activation score (z-score)") +
    ylab("") +
    theme_classic() +
    scale_colour_gradient(low = "#FF0000", high = "#F6ECEB") +
    labs(size="# of molecules", colour="-log(p-value)") +
    theme(axis.text.y = element_text(size = 18),
          axis.text.x = element_text(size = 16),
          axis.title.x = element_text(size = 16)) 

ggsave(file.path(out_folder, "08", "IPA_TCGA_dis_pat_plot_top.png"), last_plot(), device = "png", width = 10, height = 5)

```

## GSEA (pathfindr - KEGG)

```{r Plot TCGAFnveryhigh RA_Output, fig.width=8, fig.height=4}

TCGA_RA_output <- read.csv(file.path(data_folder, "08", "midica_pathseq_results", "GSEA", "TCGA_Fuso-high_RA_output_Kegg.csv"))

Extracted_pathways_TCGA <- c("Calcium signaling pathway",
                             "Bacterial invasion of epithelial cells",
                             "Cholesterol metabolism",
                             "Focal adhesion",
                             "Regulation of actin cytoskeleton",
                             "Proteoglycans in cancer",
                             "Tight junction",
                             "Leukocyte transendothelial migration",
                             "Estrogen signaling pathway",
                             "Vascular smooth muscle contraction")

TCGA_RA_output %>% 
  mutate(Term_Description = fct_reorder(Term_Description, Fold_Enrichment)) %>% 
  mutate(n_up = lengths(strsplit(.$Up_regulated, ","))) %>% 
  mutate(n_down = lengths(strsplit(.$Down_regulated, ","))) %>%  # Only upregulated genes found
  mutate(ratio_updown = log(n_up/n_down)) %>%
  mutate(n_both = n_up+n_down) -> TCGA_RA_output_plot
# EK: fixed output variable name which seemed wrong (RA_output_plot)

TCGA_RA_output_plot %>% 
  filter(Term_Description %in% Extracted_pathways_TCGA | str_detect(Term_Description, "HIF")) %>%
  top_n(10, highest_p) %>% 
  ggplot(aes(x = Fold_Enrichment, y = Term_Description, size = n_up, color = highest_p)) +
  geom_point() + 
  xlab("Fold Enrichment") +
  ylab("") +
  theme_classic() +
  scale_colour_gradient(low = "#FF0000", high = "#F6ECEB") +
  labs(size="# genes", color = "p-value") +
  theme(axis.text.y = element_text(size = 16),
        axis.text.x = element_text(size = 14),
  axis.title.x = element_text(size = 14)) 

ggsave(file.path(out_folder, "08", "KEGG_TCGA_Fuso-very_high_plot_top.png"), last_plot(), device = "png", width = 8, height = 5)

```

# EGA

## Canonical pathways

```{r IPA visualization (EGA), fig.width=10, fig.height=5}

Extracted_pathways_EGA <- c("Oxidative Phosphorylation",
                            "EIF2 Signaling",
                            "Role of BRCA1 in DNA Damage Response",
                            "LXR/RXR Activation",
                            "CDK5 Signaling",
                            "Notch Signaling",
                            "Th17 Activation Pathway",
                            "NF-κB Signaling",
                            "Colorectal Cancer Metastasis Signaling",
                            "Valine Degradation I",
                            "Xenobiotic Metabolism AHR Signaling Pathway",
                            "Aryl Hydrocarbon Receptor Signaling",
                            "Focal Adhesion")

ipa_ega <- read_excel(file.path(data_folder, "08", "IPA", "Full_EGA_fuso_load_high_no_new_075.xls"), skip = 1) %>%
  dplyr::select(diseasefun = "Ingenuity Canonical Pathways", 
                zscore = "z-score", padj = "-log(p-value)", Molecules) %>%
  mutate(zscore = ifelse(is.na(zscore), 0, zscore)) %>% 
  mutate(molecules = lengths(strsplit(.$Molecules, ","))) 

ipa_ega %>% mutate(diseasefun = fct_reorder(diseasefun, zscore)) %>%
    filter(diseasefun %in% Extracted_pathways_EGA | str_detect(diseasefun, "HIF")) %>%
    ggplot(aes(x = zscore, y = diseasefun, size = molecules, color = padj)) +
    geom_point() + 
    xlab("Activation score (z-score)") +
    ylab("") +
    theme_classic() +
    scale_colour_gradient(low = "#FF0000", high = "#F6ECEB") +
    labs(size="# of molecules", colour="-log(p-value)") +
    theme(axis.text.y = element_text(size = 18),
          axis.text.x = element_text(size = 16),
          axis.title.x = element_text(size = 16)) 

ggsave(file.path(out_folder, "08", "IPA_EGA_dis_pat_plot_top.png"), last_plot(), device = "png", width = 10, height = 5)

```

## GSEA (pathfindr - KEGG)

```{r Plot EGA_Fnveryhigh RA_Output, fig.width=10, fig.height=5}

# Extracted_pathways_EGA <- c("Signaling pathways regulating pluripotency of stem cells",
#                         "Regulation of actin cytoskeleton",
#                         "cAMP signaling pathway",
#                         "HIF-1 signaling pathway",
#                         "Insulin resistance",
#                         "Focal adhesion",
#                         "T cell receptor signaling pathway",
#                         "Mismatch repair",
#                         "Ubiquitin mediated proteolysis",
#                         "Platelet activation",
#                         "Vascular smooth muscle contraction",
#                         "Inositol phosphate metabolism",
#                         "Hippo signaling pathway",
#                         "Insulin signaling pathwa",
#                         "Hepatocellular carcinomav",
#                         "Aryl Hydrocarbon Receptor Signaling")

EGA_RA_output <- read.csv(file.path(data_folder, "08", "midica_pathseq_results", "GSEA", "EGA_Fuso-high_RA_output_Kegg.csv"))     
  #filter(Term_Description %in% Extracted_pathways_EGA)

EGA_RA_output %>% 
  mutate(Term_Description = fct_reorder(Term_Description, Fold_Enrichment)) %>% 
  mutate(n_up = lengths(strsplit(.$Up_regulated, ","))) %>% 
  mutate(n_down = lengths(strsplit(.$Down_regulated, ","))) %>% 
  mutate(ratio_updown = log(n_up/n_down)) %>% 
  mutate(n_both = n_up+n_down) -> RA_output_plot 

RA_output_plot %>% 
  top_n(10, highest_p) %>% 
  ggplot(aes(x = Fold_Enrichment, y = Term_Description, size = n_both, color = highest_p)) +
  geom_point() + 
  xlab("Fold Enrichment") +
  ylab("") +
  theme_classic() +
  scale_colour_gradient(low = "#FF0000", high = "#F6ECEB") +
  labs(size="# genes", color = "p-value") +
  theme(axis.text.y = element_text(size = 16),
        axis.text.x = element_text(size = 14),
  axis.title.x = element_text(size = 14)) 

ggsave(file.path(out_folder, "08", "KEGG_EGA_Fuso-very_high_plot_selected.png"), last_plot(), device = "png", width = 10, height = 5)

```

# Modeling colors

```{r Colors for Modeling overview}

Met_col <- RColorBrewer::brewer.pal(6, "Reds")

val <- 1
Met_col[val]
col2rgb(Met_col[val], alpha = FALSE)[,1]

```

# Formate RNA-seq

## Diseases or Functions

```{r Plotting Formate RNA-Seq (Disease function), fig.width=10, fig.height=5}

Extracted_functions_formate <- c("Apoptosis of adenocarcinoma cell lines",
                        "Colony formation of cells",
                        "Proliferation of mesenchymal stem cells",
                        "5-fluorouracil resistance of tumor cell lines",
                        "Migration of carcinoma cells",
                        "O-GlcNAcylation of protein",
                        "Degranulation of neutrophils",
                        "Accumulation of reactive oxygen species",
                        "Migration of colorectal cancer cell lines")

f_ipa_fun <- readxl::read_xls(file.path(data_folder, "08", "IPA", "RNA-seq", "Formate", "IPA_results_functions.xls"), skip = 1)%>% 
  dplyr::select(diseasefun = "Diseases or Functions Annotation", zscore = "Activation z-score", padj = "p-value", molecules = "# Molecules") %>% 
  mutate(zscore = ifelse(is.na(zscore), 0, zscore)) 

f_ipa_fun %>% 
  filter(diseasefun %in% Extracted_functions_formate) %>% 
  mutate(diseasefun = fct_reorder(diseasefun, zscore)) %>%
  ggplot(aes(x = zscore, y = diseasefun, size = molecules, color = padj)) +
  geom_point() + 
  xlab("Activation score (z-score)") +
  ylab("") +
  theme_classic() +
  scale_colour_gradient(high = "#80bfff",
                        low = "#00264d") +
  labs(size="Involved genes", colour="p-value") +
  theme(axis.text.y = element_text(size = 18),
        axis.text.x = element_text(size = 16),
  axis.title.x = element_text(size = 16)) 

ggsave(file.path(out_folder, "08", "Formate_IPA_dis_func_plot_top.png"), last_plot(), device = "png", width = 10, height = 5)

```

## Canonical Pathways

```{r Plotting Formate RNA-Seq (Pathways), fig.width=10, fig.height=5}

Extracted_pathways_formate <- c("Insulin Receptor Signaling",
                                "mTOR Signaling",
                                "Estrogen Receptor Signaling",
                                "Sirtuin Signaling Pathway",
                                "VEGF Signaling",
                                "Aryl Hydrocarbon Receptor Signaling",
                                "Superpathway of Serine and Glycine Biosynthesis I",
                                "Triacylglycerol Biosynthesis",
                                "Xenobiotic Metabolism AHR Signaling Pathway",
                                "LPS/IL-1 Mediated Inhibition of RXR Function")

f_ipa_pat <- readxl::read_xls(file.path(data_folder, "08", "IPA", "RNA-seq", "Formate", "IPA_results_pathways.xls"), skip = 1)  %>% 
  mutate(Number_of_molecules = lengths(strsplit(.$Molecules, ","))) %>% 
  dplyr::select(diseasepat = "Ingenuity Canonical Pathways", zscore = "z-score", padj = "-log(p-value)", molecules = "Number_of_molecules") %>% 
  mutate(zscore = ifelse(is.na(zscore), 0, zscore)) %>% 
  filter(padj >= 0.01)

f_ipa_pat %>% 
  filter(diseasepat %in% Extracted_pathways_formate) %>% 
  mutate(diseasepat = ifelse(diseasepat == "Superpathway of Serine and Glycine Biosynthesis I", "Serine and Glycine Biosynthesis I", diseasepat)) %>% 
  mutate(diseasepat = fct_reorder(diseasepat, padj)) -> f_ipa_pat_plot

f_ipa_pat_plot %>% 
  ggplot(aes(x = zscore, y = diseasepat, size = molecules, color = padj)) +
  geom_point() + 
  xlab("Activation score (z-score)") +
  ylab("") +
  theme_classic() +
  scale_colour_gradient(high = "#80bfff",
                       low = "#00264d") +
  labs(size = "Involved genes", colour = "-log(p-value)") +
  theme(axis.text.y = element_text(size = 18),
        axis.text.x = element_text(size = 16),
  axis.title.x = element_text(size = 16)) 

ggsave(file.path(out_folder, "08", "Formate_IPA_dis_pat_plot_top.png"), last_plot(), device = "png", width = 10, height = 5)

```

