% creates models of Fusobacterium nucleatum subsp nucleatum ATCC 25586 (FN)
% and Gemella morbillorum M424 (GM)

reactionDatabase = readtable('ReactionDatabase.txt', 'Delimiter', 'tab','TreatAsEmpty',['UND. -60001','UND. -2011','UND. -62011'], 'ReadVariableNames', false);
reactionDatabase=table2cell(reactionDatabase);

% just FN
load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
% run through the creating framework function to ensure same reaction
% namespace as joint models
models{1,1}=model;
nameTagsModels{1,1}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_';
[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels);
model=modelJoint;
save('model_FN','model');
% just GM
load('Gemella_morbillorum_M424.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
% run through the creating framework function to ensure same reaction
% namespace as joint models
models{1,1}=model;
nameTagsModels{1,1}='Gemella_morbillorum_M424_';
[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels);
model=modelJoint;
save('model_GM','model');
% FN + GM
load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{1,1}=model;
load('Gemella_morbillorum_M424.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{2,1}=model;
nameTagsModels{1,1}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_';
nameTagsModels{2,1}='Gemella_morbillorum_M424_';
% no host - host entry field empty
[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',modelJoint.rxns)),'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',400,0.01);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Gemella_morbillorum_M424_',modelJoint.rxns)),'Gemella_morbillorum_M424_biomass0',400,0.01);
model=modelJoint;
save('modelJoint_FN_GM','model');

%% with host-T18 model
clear all
% just the T18 model
load('T18_DMEM.mat');

% run through the creating framework function to ensure same reaction
% namespace as joint models
modelHost=model;

load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');
models{1,1}=model;
nameTagsModels{1,1}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_';
[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels,modelHost,'Host_');
rmRxns=modelJoint.rxns(find(strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',modelJoint.rxns)));
modelJoint=removeRxns(modelJoint,rmRxns);
% make constraints for intestinal barrier
modelJoint = changeRxnBounds(modelJoint,modelJoint.rxns(strmatch('Host_IEX',modelJoint.rxns)),0,'u');
% % exception for mets human secretes into mucus/ lumen
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_chol[u]tr';'Host_IEX_galam[u]tr';'Host_IEX_fuc-L[u]tr';'Host_IEX_etha[u]tr';'Host_IEX_drib[u]tr';'Host_IEX_na1[u]tr';'Host_IEX_h[u]tr';'Host_IEX_tag_hs[u]tr';'Host_IEX_mag_hs[u]tr';'Host_IEX_lpchol_hs[u]tr';'Host_IEX_Rtotal3[u]tr';'Host_IEX_Rtotal2[u]tr';'Host_IEX_Rtotal[u]tr';'Host_IEX_dag_hs[u]tr';'Host_IEX_chsterol[u]tr';'Host_IEX_ha_pre1[u]tr';'Host_IEX_ha[u]tr';'Host_IEX_cspg_a[u]tr';'Host_IEX_cspg_b[u]tr';'Host_IEX_cspg_c[u]tr';'Host_IEX_cspg_d[u]tr';'Host_IEX_cspg_e[u]tr';'Host_IEX_hspg[u]tr';'Host_IEX_gchola[u]tr';'Host_IEX_tdchola[u]tr';'Host_IEX_tchola[u]tr';'Host_IEX_tdechola[u]tr'},1000,'u');
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_no[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_s2l2fn2m2masn[u]tr';'Host_IEX_s2l2n2m2masn[u]tr';'Host_IEX_f1a[u]tr';'Host_IEX_gncore1[u]tr';'Host_IEX_gncore2[u]tr';'Host_IEX_dsT_antigen[u]tr';'Host_IEX_sTn_antigen[u]tr';'Host_IEX_core8[u]tr';'Host_IEX_core7[u]tr';'Host_IEX_core5[u]tr';'Host_IEX_core4[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_oh1[u]tr';'Host_IEX_co2[u]tr';'Host_IEX_hco3[u]tr';'Host_IEX_ca2[u]tr';'Host_IEX_cl[u]tr';'Host_IEX_k[u]tr'},1000,'u');
% %
model=modelJoint;
% DMEM supply to humen cell on the basal side
model=useREFPepDietPlus_Host(model);
save('model_T18','model');

% FN + human
load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{1,1}=model;
nameTagsModels{1,1}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_';
load('T18_DMEM.mat');
 
modelHost=model;

[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels,modelHost,'Host_');
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',modelJoint.rxns)),'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',400,0.01);
hostRxns=strmatch('Host_',modelJoint.rxns);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(hostRxns(1,1):hostRxns(length(hostRxns),1)),'Host_biomass_reaction' ,400,0.01);
% make constraints for intestinal barrier
modelJoint = changeRxnBounds(modelJoint,modelJoint.rxns(strmatch('Host_IEX',modelJoint.rxns)),0,'u');
% % exception for mets human secretes into mucus/ lumen
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_chol[u]tr';'Host_IEX_galam[u]tr';'Host_IEX_fuc-L[u]tr';'Host_IEX_etha[u]tr';'Host_IEX_drib[u]tr';'Host_IEX_na1[u]tr';'Host_IEX_h[u]tr';'Host_IEX_tag_hs[u]tr';'Host_IEX_mag_hs[u]tr';'Host_IEX_lpchol_hs[u]tr';'Host_IEX_Rtotal3[u]tr';'Host_IEX_Rtotal2[u]tr';'Host_IEX_Rtotal[u]tr';'Host_IEX_dag_hs[u]tr';'Host_IEX_chsterol[u]tr';'Host_IEX_ha_pre1[u]tr';'Host_IEX_ha[u]tr';'Host_IEX_cspg_a[u]tr';'Host_IEX_cspg_b[u]tr';'Host_IEX_cspg_c[u]tr';'Host_IEX_cspg_d[u]tr';'Host_IEX_cspg_e[u]tr';'Host_IEX_hspg[u]tr';'Host_IEX_gchola[u]tr';'Host_IEX_tdchola[u]tr';'Host_IEX_tchola[u]tr';'Host_IEX_tdechola[u]tr'},1000,'u');
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_no[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_s2l2fn2m2masn[u]tr';'Host_IEX_s2l2n2m2masn[u]tr';'Host_IEX_f1a[u]tr';'Host_IEX_gncore1[u]tr';'Host_IEX_gncore2[u]tr';'Host_IEX_dsT_antigen[u]tr';'Host_IEX_sTn_antigen[u]tr';'Host_IEX_core8[u]tr';'Host_IEX_core7[u]tr';'Host_IEX_core5[u]tr';'Host_IEX_core4[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_oh1[u]tr';'Host_IEX_co2[u]tr';'Host_IEX_hco3[u]tr';'Host_IEX_ca2[u]tr';'Host_IEX_cl[u]tr';'Host_IEX_k[u]tr'},1000,'u');
% %
model=modelJoint;
% DMEM supply to humen cell on the basal side
model=useREFPepDietPlus_Host(model);
save('modelJoint_T18_FN','model');

% GM + human
load('Gemella_morbillorum_M424.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{1,1}=model;
nameTagsModels{1,1}='Gemella_morbillorum_M424_';
load('T18_DMEM.mat');
 
modelHost=model;

[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels,modelHost,'Host_');
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Gemella_morbillorum_M424_',modelJoint.rxns)),'Gemella_morbillorum_M424_biomass0',400,0.01);
hostRxns=strmatch('Host_',modelJoint.rxns);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(hostRxns(1,1):hostRxns(length(hostRxns),1)),'Host_biomass_reaction' ,400,0.01);
% make constraints for intestinal barrier
modelJoint = changeRxnBounds(modelJoint,modelJoint.rxns(strmatch('Host_IEX',modelJoint.rxns)),0,'u');
% % exception for mets human secretes into mucus/ lumen
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_chol[u]tr';'Host_IEX_galam[u]tr';'Host_IEX_fuc-L[u]tr';'Host_IEX_etha[u]tr';'Host_IEX_drib[u]tr';'Host_IEX_na1[u]tr';'Host_IEX_h[u]tr';'Host_IEX_tag_hs[u]tr';'Host_IEX_mag_hs[u]tr';'Host_IEX_lpchol_hs[u]tr';'Host_IEX_Rtotal3[u]tr';'Host_IEX_Rtotal2[u]tr';'Host_IEX_Rtotal[u]tr';'Host_IEX_dag_hs[u]tr';'Host_IEX_chsterol[u]tr';'Host_IEX_ha_pre1[u]tr';'Host_IEX_ha[u]tr';'Host_IEX_cspg_a[u]tr';'Host_IEX_cspg_b[u]tr';'Host_IEX_cspg_c[u]tr';'Host_IEX_cspg_d[u]tr';'Host_IEX_cspg_e[u]tr';'Host_IEX_hspg[u]tr';'Host_IEX_gchola[u]tr';'Host_IEX_tdchola[u]tr';'Host_IEX_tchola[u]tr';'Host_IEX_tdechola[u]tr'},1000,'u');
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_no[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_s2l2fn2m2masn[u]tr';'Host_IEX_s2l2n2m2masn[u]tr';'Host_IEX_f1a[u]tr';'Host_IEX_gncore1[u]tr';'Host_IEX_gncore2[u]tr';'Host_IEX_dsT_antigen[u]tr';'Host_IEX_sTn_antigen[u]tr';'Host_IEX_core8[u]tr';'Host_IEX_core7[u]tr';'Host_IEX_core5[u]tr';'Host_IEX_core4[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_oh1[u]tr';'Host_IEX_co2[u]tr';'Host_IEX_hco3[u]tr';'Host_IEX_ca2[u]tr';'Host_IEX_cl[u]tr';'Host_IEX_k[u]tr'},1000,'u');
% %
model=modelJoint;
% DMEM supply to humen cell on the basal side
model=useREFPepDietPlus_Host(model);
save('modelJoint_T18_GM','model');

% human + FN + GM
load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{1,1}=model;
load('Gemella_morbillorum_M424.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{2,1}=model;
nameTagsModels{1,1}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_';
nameTagsModels{2,1}='Gemella_morbillorum_M424_';
load('T18_DMEM.mat');
 
modelHost=model;

[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels,modelHost,'Host_');
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',modelJoint.rxns)),'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',400,0.01);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Gemella_morbillorum_M424_',modelJoint.rxns)),'Gemella_morbillorum_M424_biomass0',400,0.01);
hostRxns=strmatch('Host_',modelJoint.rxns);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(hostRxns(1,1):hostRxns(length(hostRxns),1)),'Host_biomass_reaction' ,400,0.01);
% make constraints for intestinal barrier
modelJoint = changeRxnBounds(modelJoint,modelJoint.rxns(strmatch('Host_IEX',modelJoint.rxns)),0,'u');
% % exception for mets human secretes into mucus/ lumen
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_chol[u]tr';'Host_IEX_galam[u]tr';'Host_IEX_fuc-L[u]tr';'Host_IEX_etha[u]tr';'Host_IEX_drib[u]tr';'Host_IEX_na1[u]tr';'Host_IEX_h[u]tr';'Host_IEX_tag_hs[u]tr';'Host_IEX_mag_hs[u]tr';'Host_IEX_lpchol_hs[u]tr';'Host_IEX_Rtotal3[u]tr';'Host_IEX_Rtotal2[u]tr';'Host_IEX_Rtotal[u]tr';'Host_IEX_dag_hs[u]tr';'Host_IEX_chsterol[u]tr';'Host_IEX_ha_pre1[u]tr';'Host_IEX_ha[u]tr';'Host_IEX_cspg_a[u]tr';'Host_IEX_cspg_b[u]tr';'Host_IEX_cspg_c[u]tr';'Host_IEX_cspg_d[u]tr';'Host_IEX_cspg_e[u]tr';'Host_IEX_hspg[u]tr';'Host_IEX_gchola[u]tr';'Host_IEX_tdchola[u]tr';'Host_IEX_tchola[u]tr';'Host_IEX_tdechola[u]tr'},1000,'u');
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_no[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_s2l2fn2m2masn[u]tr';'Host_IEX_s2l2n2m2masn[u]tr';'Host_IEX_f1a[u]tr';'Host_IEX_gncore1[u]tr';'Host_IEX_gncore2[u]tr';'Host_IEX_dsT_antigen[u]tr';'Host_IEX_sTn_antigen[u]tr';'Host_IEX_core8[u]tr';'Host_IEX_core7[u]tr';'Host_IEX_core5[u]tr';'Host_IEX_core4[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_oh1[u]tr';'Host_IEX_co2[u]tr';'Host_IEX_hco3[u]tr';'Host_IEX_ca2[u]tr';'Host_IEX_cl[u]tr';'Host_IEX_k[u]tr'},1000,'u');
% %
model=modelJoint;
% DMEM supply to humen cell on the basal side
model=useREFPepDietPlus_Host(model);
save('modelJoint_T18_FN_GM','model');

%% with host-control model
clear all
% just the control model
load('Control_DMEM.mat');

% run through the creating framework function to ensure same reaction
% namespace as joint models
modelHost=model;

load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');
models{1,1}=model;
nameTagsModels{1,1}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_';
[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels,modelHost,'Host_');
rmRxns=modelJoint.rxns(find(strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',modelJoint.rxns)));
modelJoint=removeRxns(modelJoint,rmRxns);
% make constraints for intestinal barrier
modelJoint = changeRxnBounds(modelJoint,modelJoint.rxns(strmatch('Host_IEX',modelJoint.rxns)),0,'u');
% % exception for mets human secretes into mucus/ lumen
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_chol[u]tr';'Host_IEX_galam[u]tr';'Host_IEX_fuc-L[u]tr';'Host_IEX_etha[u]tr';'Host_IEX_drib[u]tr';'Host_IEX_na1[u]tr';'Host_IEX_h[u]tr';'Host_IEX_tag_hs[u]tr';'Host_IEX_mag_hs[u]tr';'Host_IEX_lpchol_hs[u]tr';'Host_IEX_Rtotal3[u]tr';'Host_IEX_Rtotal2[u]tr';'Host_IEX_Rtotal[u]tr';'Host_IEX_dag_hs[u]tr';'Host_IEX_chsterol[u]tr';'Host_IEX_ha_pre1[u]tr';'Host_IEX_ha[u]tr';'Host_IEX_cspg_a[u]tr';'Host_IEX_cspg_b[u]tr';'Host_IEX_cspg_c[u]tr';'Host_IEX_cspg_d[u]tr';'Host_IEX_cspg_e[u]tr';'Host_IEX_hspg[u]tr';'Host_IEX_gchola[u]tr';'Host_IEX_tdchola[u]tr';'Host_IEX_tchola[u]tr';'Host_IEX_tdechola[u]tr'},1000,'u');
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_no[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_s2l2fn2m2masn[u]tr';'Host_IEX_s2l2n2m2masn[u]tr';'Host_IEX_f1a[u]tr';'Host_IEX_gncore1[u]tr';'Host_IEX_gncore2[u]tr';'Host_IEX_dsT_antigen[u]tr';'Host_IEX_sTn_antigen[u]tr';'Host_IEX_core8[u]tr';'Host_IEX_core7[u]tr';'Host_IEX_core5[u]tr';'Host_IEX_core4[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_oh1[u]tr';'Host_IEX_co2[u]tr';'Host_IEX_hco3[u]tr';'Host_IEX_ca2[u]tr';'Host_IEX_cl[u]tr';'Host_IEX_k[u]tr'},1000,'u');
% %
model=modelJoint;
% DMEM supply to humen cell on the basal side
model=useREFPepDietPlus_Host(model);
save('model_Control','model');

% FN + human
load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{1,1}=model;
nameTagsModels{1,1}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_';
load('Control_DMEM.mat');

modelHost=model;

[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels,modelHost,'Host_');
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',modelJoint.rxns)),'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',400,0.01);
hostRxns=strmatch('Host_',modelJoint.rxns);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(hostRxns(1,1):hostRxns(length(hostRxns),1)),'Host_biomass_reaction',400,0.01);
% make constraints for intestinal barrier
modelJoint = changeRxnBounds(modelJoint,modelJoint.rxns(strmatch('Host_IEX',modelJoint.rxns)),0,'u');
% % exception for mets human secretes into mucus/ lumen
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_chol[u]tr';'Host_IEX_galam[u]tr';'Host_IEX_fuc-L[u]tr';'Host_IEX_etha[u]tr';'Host_IEX_drib[u]tr';'Host_IEX_na1[u]tr';'Host_IEX_h[u]tr';'Host_IEX_tag_hs[u]tr';'Host_IEX_mag_hs[u]tr';'Host_IEX_lpchol_hs[u]tr';'Host_IEX_Rtotal3[u]tr';'Host_IEX_Rtotal2[u]tr';'Host_IEX_Rtotal[u]tr';'Host_IEX_dag_hs[u]tr';'Host_IEX_chsterol[u]tr';'Host_IEX_ha_pre1[u]tr';'Host_IEX_ha[u]tr';'Host_IEX_cspg_a[u]tr';'Host_IEX_cspg_b[u]tr';'Host_IEX_cspg_c[u]tr';'Host_IEX_cspg_d[u]tr';'Host_IEX_cspg_e[u]tr';'Host_IEX_hspg[u]tr';'Host_IEX_gchola[u]tr';'Host_IEX_tdchola[u]tr';'Host_IEX_tchola[u]tr';'Host_IEX_tdechola[u]tr'},1000,'u');
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_no[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_s2l2fn2m2masn[u]tr';'Host_IEX_s2l2n2m2masn[u]tr';'Host_IEX_f1a[u]tr';'Host_IEX_gncore1[u]tr';'Host_IEX_gncore2[u]tr';'Host_IEX_dsT_antigen[u]tr';'Host_IEX_sTn_antigen[u]tr';'Host_IEX_core8[u]tr';'Host_IEX_core7[u]tr';'Host_IEX_core5[u]tr';'Host_IEX_core4[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_oh1[u]tr';'Host_IEX_co2[u]tr';'Host_IEX_hco3[u]tr';'Host_IEX_ca2[u]tr';'Host_IEX_cl[u]tr';'Host_IEX_k[u]tr'},1000,'u');
% %
model=modelJoint;
% DMEM supply to humen cell on the basal side
model=useREFPepDietPlus_Host(model);
save('modelJoint_Control_FN','model');

% GM + human
load('Gemella_morbillorum_M424.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{1,1}=model;
nameTagsModels{1,1}='Gemella_morbillorum_M424_';
load('Control_DMEM.mat');

modelHost=model;

[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels,modelHost,'Host_');
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Gemella_morbillorum_M424_',modelJoint.rxns)),'Gemella_morbillorum_M424_biomass0',400,0.01);
hostRxns=strmatch('Host_',modelJoint.rxns);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(hostRxns(1,1):hostRxns(length(hostRxns),1)),'Host_biomass_reaction',400,0.01);
% make constraints for intestinal barrier
modelJoint = changeRxnBounds(modelJoint,modelJoint.rxns(strmatch('Host_IEX',modelJoint.rxns)),0,'u');
% % exception for mets human secretes into mucus/ lumen
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_chol[u]tr';'Host_IEX_galam[u]tr';'Host_IEX_fuc-L[u]tr';'Host_IEX_etha[u]tr';'Host_IEX_drib[u]tr';'Host_IEX_na1[u]tr';'Host_IEX_h[u]tr';'Host_IEX_tag_hs[u]tr';'Host_IEX_mag_hs[u]tr';'Host_IEX_lpchol_hs[u]tr';'Host_IEX_Rtotal3[u]tr';'Host_IEX_Rtotal2[u]tr';'Host_IEX_Rtotal[u]tr';'Host_IEX_dag_hs[u]tr';'Host_IEX_chsterol[u]tr';'Host_IEX_ha_pre1[u]tr';'Host_IEX_ha[u]tr';'Host_IEX_cspg_a[u]tr';'Host_IEX_cspg_b[u]tr';'Host_IEX_cspg_c[u]tr';'Host_IEX_cspg_d[u]tr';'Host_IEX_cspg_e[u]tr';'Host_IEX_hspg[u]tr';'Host_IEX_gchola[u]tr';'Host_IEX_tdchola[u]tr';'Host_IEX_tchola[u]tr';'Host_IEX_tdechola[u]tr'},1000,'u');
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_no[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_s2l2fn2m2masn[u]tr';'Host_IEX_s2l2n2m2masn[u]tr';'Host_IEX_f1a[u]tr';'Host_IEX_gncore1[u]tr';'Host_IEX_gncore2[u]tr';'Host_IEX_dsT_antigen[u]tr';'Host_IEX_sTn_antigen[u]tr';'Host_IEX_core8[u]tr';'Host_IEX_core7[u]tr';'Host_IEX_core5[u]tr';'Host_IEX_core4[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_oh1[u]tr';'Host_IEX_co2[u]tr';'Host_IEX_hco3[u]tr';'Host_IEX_ca2[u]tr';'Host_IEX_cl[u]tr';'Host_IEX_k[u]tr'},1000,'u');
% %
model=modelJoint;
% DMEM supply to humen cell on the basal side
model=useREFPepDietPlus_Host(model);
save('modelJoint_Control_GM','model');

% human + FN + GM
load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{1,1}=model;
load('Gemella_morbillorum_M424.mat');
bioInd=model.rxns(strncmp(model.rxns,'biomass',7));
model.rxns= regexprep(model.rxns,bioInd,'biomass0');
models{2,1}=model;
nameTagsModels{1,1}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_';
nameTagsModels{2,1}='Gemella_morbillorum_M424_';
load('Control_DMEM.mat');

modelHost=model;

[modelJoint] = createMultipleSpeciesModel(models,nameTagsModels,modelHost,'Host_');
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',modelJoint.rxns)),'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',400,0.01);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(strmatch('Gemella_morbillorum_M424_',modelJoint.rxns)),'Gemella_morbillorum_M424_biomass0',400,0.01);
hostRxns=strmatch('Host_',modelJoint.rxns);
[modelJoint]=coupleRxnList2Rxn(modelJoint,modelJoint.rxns(hostRxns(1,1):hostRxns(length(hostRxns),1)),'Host_biomass_reaction',400,0.01);
% make constraints for intestinal barrier
modelJoint = changeRxnBounds(modelJoint,modelJoint.rxns(strmatch('Host_IEX',modelJoint.rxns)),0,'u');
% % exception for mets human secretes into mucus/ lumen
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_chol[u]tr';'Host_IEX_galam[u]tr';'Host_IEX_fuc-L[u]tr';'Host_IEX_etha[u]tr';'Host_IEX_drib[u]tr';'Host_IEX_na1[u]tr';'Host_IEX_h[u]tr';'Host_IEX_tag_hs[u]tr';'Host_IEX_mag_hs[u]tr';'Host_IEX_lpchol_hs[u]tr';'Host_IEX_Rtotal3[u]tr';'Host_IEX_Rtotal2[u]tr';'Host_IEX_Rtotal[u]tr';'Host_IEX_dag_hs[u]tr';'Host_IEX_chsterol[u]tr';'Host_IEX_ha_pre1[u]tr';'Host_IEX_ha[u]tr';'Host_IEX_cspg_a[u]tr';'Host_IEX_cspg_b[u]tr';'Host_IEX_cspg_c[u]tr';'Host_IEX_cspg_d[u]tr';'Host_IEX_cspg_e[u]tr';'Host_IEX_hspg[u]tr';'Host_IEX_gchola[u]tr';'Host_IEX_tdchola[u]tr';'Host_IEX_tchola[u]tr';'Host_IEX_tdechola[u]tr'},1000,'u');
modelJoint=changeRxnBounds(modelJoint,{'Host_IEX_no[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_n2m2nmasn[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_s2l2fn2m2masn[u]tr';'Host_IEX_s2l2n2m2masn[u]tr';'Host_IEX_f1a[u]tr';'Host_IEX_gncore1[u]tr';'Host_IEX_gncore2[u]tr';'Host_IEX_dsT_antigen[u]tr';'Host_IEX_sTn_antigen[u]tr';'Host_IEX_core8[u]tr';'Host_IEX_core7[u]tr';'Host_IEX_core5[u]tr';'Host_IEX_core4[u]tr';'Host_IEX_s2l2n2m2m[u]tr';'Host_IEX_oh1[u]tr';'Host_IEX_co2[u]tr';'Host_IEX_hco3[u]tr';'Host_IEX_ca2[u]tr';'Host_IEX_cl[u]tr';'Host_IEX_k[u]tr'},1000,'u');
% %
model=modelJoint;
% DMEM supply to humen cell on the basal side
model=useREFPepDietPlus_Host(model);
save('modelJoint_Control_FN_GM','model');

