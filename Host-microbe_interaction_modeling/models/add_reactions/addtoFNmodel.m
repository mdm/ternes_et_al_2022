% add to model
load('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat');

metaboliteDatabase = readtable('MetaboliteDatabase.txt', 'Delimiter', 'tab', 'ReadVariableNames', false);
metaboliteDatabase=table2cell(metaboliteDatabase);
database.metabolites=metaboliteDatabase;
for i=1:size(database.metabolites,1)
    database.metabolites{i,5}=num2str(database.metabolites{i,5});
end
reactionDatabase = readtable('ReactionDatabase.txt', 'Delimiter', 'tab', 'ReadVariableNames', false);
reactionDatabase=table2cell(reactionDatabase);
database.reactions=reactionDatabase;

addRxns={
    'EX_alalys(e)'
    'EX_alaval(e)'
    'EX_alaile(e)'
    'ALALYSabc'
    'ALAVALabc'
    'ALAILEabc'
    'ALALYS1c'
    'ALAVAL1c'
    'ALAILE1c'
    'PTRCAT1'
    'EX_aprut(e)'
    'APRUTtex'
    '2HBt2'
    'EX_2hb(e)'
    '5OXPRO_FEs'
    'EX_5oxpro(e)'
    'EX_lac_L(e)'
    'L_LACt2r'
    };
for j = 1:length(addRxns)
    if ~any(ismember(model.rxns, addRxns{j}))
        formula = database.reactions{ismember(database.reactions(:, 1), addRxns{j}), 3};
        model = addReaction(model, addRxns{j}, 'reactionFormula', formula, 'geneRule', '');
    end
end
model = rebuildModel(model);
save('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586.mat','model');