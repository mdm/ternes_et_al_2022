%% normalize the min fluxes, max fluxes, and flux spans for plot


%% flux spans
load('FluxSpans.mat');
FluxSpansNormalized={};

% first give the models names easier to understand
for i=7:size(FluxSpans,2)
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'model_FN','FN');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'model_GM','GM');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'modelJoint_FN_GM','FN + GM');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'model_Control','Control');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'model_COAD','Caco-2');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'modelJoint_COAD_FN_GM','Caco-2 + FN + GM');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'modelJoint_Control_FN_GM','Control + FN + GM');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'modelJoint_COAD_FN','Caco-2 + FN');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'modelJoint_COAD_GM','Caco-2 + GM');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'modelJoint_Control_FN','Control + FN');
    FluxSpans{1,i}=strrep(FluxSpans{1,i},'modelJoint_Control_GM','Control + GM');
end

colCnt=2;
for i=7:size(FluxSpans,2)
    FluxSpansNormalized{1,colCnt}=FluxSpans{1,i};
    colCnt=colCnt+1;
end

rowCnt=2;
for i=2:size(FluxSpans,1)
    allVals=[];
    cnt=1;
    for k=7:size(FluxSpans,2)
        allVals(cnt,1)=FluxSpans{i,k};
        cnt=cnt+1;
    end
    % only the reactions that have a non-zero flux span in any condition
    if max(allVals) > 0.00000001
        FluxSpansNormalized{rowCnt,1}=FluxSpans{i,1};
        colCnt=2;
        for k=7:size(FluxSpans,2)
            FluxSpansNormalized{rowCnt,colCnt}=FluxSpans{i,k}/max(allVals);
            colCnt=colCnt+1;
        end
        rowCnt=rowCnt+1;
    end
end
save('FluxSpansNormalized','FluxSpansNormalized');

%% max fluxes
load('MaxFluxes.mat');
MaxFluxesNormalized={};

% first give the models names easier to understand
for i=7:size(MaxFluxes,2)
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'model_FN','FN');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'model_GM','GM');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'modelJoint_FN_GM','FN + GM');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'model_Control','Control');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'model_COAD','Caco-2');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'modelJoint_COAD_FN_GM','Caco-2 + FN + GM');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'modelJoint_Control_FN_GM','Control + FN + GM');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'modelJoint_COAD_FN','Caco-2 + FN');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'modelJoint_COAD_GM','Caco-2 + GM');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'modelJoint_Control_FN','Control + FN');
    MaxFluxes{1,i}=strrep(MaxFluxes{1,i},'modelJoint_Control_GM','Control + GM');
end

colCnt=2;
for i=7:size(MaxFluxes,2)
    MaxFluxesNormalized{1,colCnt}=MaxFluxes{1,i};
    colCnt=colCnt+1;
end

rowCnt=2;
for i=2:size(MaxFluxes,1)
    allVals=[];
    cnt=1;
    for k=7:size(MaxFluxes,2)
        allVals(cnt,1)=MaxFluxes{i,k};
        cnt=cnt+1;
    end
% if minimal flux is zero or positive and maximal flux is positive
    if (min(allVals) > 0.00000001 || abs(min(allVals)) < 0.00000001) && max(allVals) > 0.00000001
        MaxFluxesNormalized{rowCnt,1}=MaxFluxes{i,1};
        colCnt=2;
        for k=7:size(MaxFluxes,2)
            MaxFluxesNormalized{rowCnt,colCnt}=MaxFluxes{i,k}/max(allVals);
            % set very low values to zero
            if abs(MaxFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                MaxFluxesNormalized{rowCnt,colCnt}=0;
            end
            colCnt=colCnt+1;
        end
        rowCnt=rowCnt+1;
%         % if minimal flux is negative and maximal flux is zero or
%         % negative
%     elseif min(allVals) < -0.00000001 && (max(allVals) < -0.00000001 || abs(max(allVals)) < 0.00000001)
%         MaxFluxesNormalized{rowCnt,1}=MaxFluxes{i,1};
%         colCnt=2;
%         for k=7:size(MaxFluxes,2)
%             MaxFluxesNormalized{rowCnt,colCnt}=-(MaxFluxes{i,k}/min(allVals));
%             % set very low values to zero
%             if abs(MaxFluxesNormalized{rowCnt,colCnt}) < 0.00000001
%                 MinFluxesNormalized{rowCnt,colCnt}=0;
%             end
%             colCnt=colCnt+1;
%         end
%         rowCnt=rowCnt+1;
        % if minimal flux is negative and maximal flux is positive
    elseif min(allVals) < -0.00000001 && max(allVals) > 0.00000001
        MaxFluxesNormalized{rowCnt,1}=MaxFluxes{i,1};
        colCnt=2;
        for k=7:size(MaxFluxes,2)
            if MaxFluxes{i,k} < -0.00000001
                MaxFluxesNormalized{rowCnt,colCnt}=-(MaxFluxes{i,k}/min(allVals));
                % set very low values to zero
                if abs(MaxFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                    MaxFluxesNormalized{rowCnt,colCnt}=0;
                end
                colCnt=colCnt+1;
            elseif MaxFluxes{i,k} > 0.00000001
                MaxFluxesNormalized{rowCnt,colCnt}=MaxFluxes{i,k}/max(allVals);
                % set very low values to zero
                if abs(MaxFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                    MaxFluxesNormalized{rowCnt,colCnt}=0;
                end
                colCnt=colCnt+1;
            elseif abs(MaxFluxes{i,k}) < 0.00000001
                MaxFluxesNormalized{rowCnt,colCnt}=MaxFluxes{i,k};
                % set very low values to zero
                if abs(MaxFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                    MaxFluxesNormalized{rowCnt,colCnt}=0;
                end
                colCnt=colCnt+1;
            end
        end
        rowCnt=rowCnt+1;
    end
end
save('MaxFluxesNormalized','MaxFluxesNormalized');

%% min fluxes
load('MinFluxes.mat');
MinFluxesNormalized={};

% first give the models names easier to understand
for i=7:size(MinFluxes,2)
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'model_FN','FN');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'model_GM','GM');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'modelJoint_FN_GM','FN + GM');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'model_Control','Control');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'model_COAD','Caco-2');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'modelJoint_COAD_FN_GM','Caco-2 + FN + GM');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'modelJoint_Control_FN_GM','Control + FN + GM');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'modelJoint_COAD_FN','Caco-2 + FN');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'modelJoint_COAD_GM','Caco-2 + GM');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'modelJoint_Control_FN','Control + FN');
    MinFluxes{1,i}=strrep(MinFluxes{1,i},'modelJoint_Control_GM','Control + GM');
end

colCnt=2;
for i=7:size(MinFluxes,2)
    MinFluxesNormalized{1,colCnt}=MinFluxes{1,i};
    colCnt=colCnt+1;
end

rowCnt=2;
for i=2:size(MinFluxes,1)
    allVals=[];
    cnt=1;
    for k=7:size(MinFluxes,2)
        allVals(cnt,1)=MinFluxes{i,k};
        cnt=cnt+1;
    end
    % if minimal flux is zero or positive and maximal flux is positive
    if (min(allVals) > 0.00000001 || abs(min(allVals)) < 0.00000001) && max(allVals) > 0.00000001
        MinFluxesNormalized{rowCnt,1}=MinFluxes{i,1};
        colCnt=2;
        for k=7:size(MinFluxes,2)
            MinFluxesNormalized{rowCnt,colCnt}=MinFluxes{i,k}/max(allVals);
            % set very low values to zero
            if abs(MinFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                MinFluxesNormalized{rowCnt,colCnt}=0;
            end
            colCnt=colCnt+1;
        end
        rowCnt=rowCnt+1;
        % if minimal flux is negative and maximal flux is zero or negative
    elseif min(allVals) < -0.00000001 && (max(allVals) < -0.00000001 || abs(max(allVals)) < 0.00000001)
        MinFluxesNormalized{rowCnt,1}=MinFluxes{i,1};
        colCnt=2;
        for k=7:size(MinFluxes,2)
            MinFluxesNormalized{rowCnt,colCnt}=-(MinFluxes{i,k}/min(allVals));
            % set very low values to zero
            if abs(MinFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                MinFluxesNormalized{rowCnt,colCnt}=0;
            end
            colCnt=colCnt+1;
        end
        rowCnt=rowCnt+1;
        % if minimal flux is negative and maximal flux is positive
    elseif min(allVals) < -0.00000001 && max(allVals) > 0.00000001
        MinFluxesNormalized{rowCnt,1}=MinFluxes{i,1};
        colCnt=2;
        for k=7:size(MinFluxes,2)
            if MinFluxes{i,k} < -0.00000001
                MinFluxesNormalized{rowCnt,colCnt}=MinFluxes{i,k}/min(allVals);
                % set very low values to zero
                if abs(MinFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                    MinFluxesNormalized{rowCnt,colCnt}=0;
                end
                colCnt=colCnt+1;
            elseif MinFluxes{i,k} > 0.00000001
                MinFluxesNormalized{rowCnt,colCnt}=MinFluxes{i,k}/max(allVals);
                % set very low values to zero
                if abs(MinFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                    MinFluxesNormalized{rowCnt,colCnt}=0;
                end
                colCnt=colCnt+1;
            elseif abs(MinFluxes{i,k}) < 0.00000001
                MinFluxesNormalized{rowCnt,colCnt}=MinFluxes{i,k};
                % set very low values to zero
                if abs(MinFluxesNormalized{rowCnt,colCnt}) < 0.00000001
                    MinFluxesNormalized{rowCnt,colCnt}=0;
                end
                colCnt=colCnt+1;
            end
        end
         rowCnt=rowCnt+1;
    end
end
save('MinFluxesNormalized','MinFluxesNormalized');

clear all
% now extract from the tables the data that should be plotted
load('MaxFluxesNormalized.mat');
load('MetaboliteNames.mat');
% host body fluid exchanges
HostBodyFluids_MaxFluxes={};
cnt=2;
for i=1:size(MaxFluxesNormalized,2)
    HostBodyFluids_MaxFluxes{1,i}=MaxFluxesNormalized{1,i};
end
for i=2:size(MaxFluxesNormalized,1)
    if strncmp('Host_EX_',MaxFluxesNormalized{i,1},8)
        HostBodyFluids_MaxFluxes{cnt,1}=MetaboliteNames(find(strcmp(MaxFluxesNormalized{i,1},MetaboliteNames(:,1))),2);
        for j=2:size(MaxFluxesNormalized,2)
            HostBodyFluids_MaxFluxes{cnt,j}=MaxFluxesNormalized{i,j};
        end
        cnt=cnt+1;
    end
end

LumenExchanges_MaxFluxes={};
cnt=2;
for i=1:size(MaxFluxesNormalized,2)
    LumenExchanges_MaxFluxes{1,i}=MaxFluxesNormalized{1,i};
end
for i=2:size(MaxFluxesNormalized,1)
    if strncmp('EX_',MaxFluxesNormalized{i,1},3)
        LumenExchanges_MaxFluxes{cnt,1}=MetaboliteNames(find(strcmp(MaxFluxesNormalized{i,1},MetaboliteNames(:,1))),2);
        for j=2:size(MaxFluxesNormalized,2)
            LumenExchanges_MaxFluxes{cnt,j}=MaxFluxesNormalized{i,j};
        end
        cnt=cnt+1;
    end
end
