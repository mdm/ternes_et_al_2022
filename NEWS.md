NEWS
================

## 2022-02-14: Add pipeline to render figures

- Add targets pipeline to render the figures from processed source data files.
- Update README with informations on how to render these figures.
- Include private figshare link to download the figures data files.

## 2022-02-12: Put the repository online

- Repository is made available
- All required data files were collected (except a missing file in `03_Ternes_et_al_TCGA_analysis_GSEA.Rmd` which is not of interest according to Dominik Ternes) and will be uploaded to figshare soon.

## 2022-02-10: Consolidate paths

- Identify input and output data filepaths in `Rmarkdown` files and move/save them into a subfolder of the current repository.
- Fix a conditional test error identified in November 2021.

## 2022-02-09: Migrate the repository

In order to setup the new `ternes_et_al_2022` repository, the already included binary data were removed and will be made available on [figshare](https://figshare.com/).
The following procedure was applied:

- Fetching Dominik Ternes' latest commit (`ternes_et_al_2021` gitlab repository, original hash `fd156a54` 2021-07-01)
- Rewriting the git history using [BFG Repo-Cleaner](https://rtyley.github.io/bfg-repo-cleaner/) ([bfg-1.14.0.jar](https://repo1.maven.org/maven2/com/madgag/bfg/1.14.0/bfg-1.14.0.jar)) to remove already pushed binary data (repository hashes up to `b489bad0` were not affected, latest commit: `fd156a54` -> `4d00a42e`)
  ``` bash
  git clone --mirror ssh://git@gitlab.lcsb.uni.lu:8022/mdm/ternes_et_al_2022.git
  java -jar ./bfg-1.14.0.jar --delete-folders Data --no-blob-protection ./ternes_et_al_2022.git
  git reflog expire --expire=now --all && git gc --prune=now --aggressive ./ternes_et_al_2022.git/
  ```
