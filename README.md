Ternes et al., 2022
================

This repository provides the *R* scripts used to perform the analyses
and render the associated figures of the paper.

The data files, which are required by the scripts, will be available on
figshare.

## Main analysis files

The main analyses performed during the study were collected and provided
by Dominik Ternes in the following files:

-   [01_Ternes_et_al_16S_analysis.Rmd](./01_Ternes_et_al_16S_analysis.Rmd)
-   [02_Ternes_et_al_TCGA_analysis.Rmd](./02_Ternes_et_al_TCGA_analysis.Rmd)
-   [03_Ternes_et_al_TCGA_analysis_GSEA.Rmd](./03_Ternes_et_al_TCGA_analysis_GSEA.Rmd)
-   [04_Ternes_et_al_TCGA_analysis_stages.Rmd](./04_Ternes_et_al_TCGA_analysis_stages.Rmd)
-   [05_Ternes_et_al_EGA_analysis.Rmd](./05_Ternes_et_al_EGA_analysis.Rmd)
-   [06_Ternes_et_al_EGA_analysis_CMS.Rmd](./06_Ternes_et_al_EGA_analysis_CMS.Rmd)
-   [07_Ternes_et_al_EGA_analysis_GSEA.Rmd](./07_Ternes_et_al_EGA_analysis_GSEA.Rmd)
-   [08_Ternes_et_al_GSEA_IPA_plotting.Rmd](./08_Ternes_et_al_GSEA_IPA_plotting.Rmd)
-   [09_Ternes_et_al_HuMiX_untargeted_metabolite_analysis.Rmd](./09_Ternes_et_al_HuMiX_untargeted_metabolite_analysis.Rmd)
-   [10_Ternes_et_al_Yachida_KO_metabolite_analysis.Rmd](./10_Ternes_et_al_Yachida_KO_metabolite_analysis.Rmd)
-   [11_Ternes_et_al_Yachida_Model_HuMiX_venn.Rmd](./11_Ternes_et_al_Yachida_Model_HuMiX_venn.Rmd)
-   [12_Ternes_et_al_lsmeans_immune_cell_phenotyping.Rmd](./12_Ternes_et_al_lsmeans_immune_cell_phenotyping.Rmd)

## Figures

In order to render most of the figures showed in the manuscript, this
repository provides a dedicated [targets
markdown](https://books.ropensci.org/targets/markdown.html) file. The
script uses intermediate source data files which were isolated from the
main analyses source and data files and stored within the `data-figure`
folder (to be downloaded from figshare and extracted in the current
repository folder).

To render these figures:

-   Clone the current repository

    ``` bash
    git clone https://gitlab.lcsb.uni.lu/mdm/ternes_et_al_2022.git
    ```

-   Install the required *R* packages (in particular
    [`targets`](https://github.com/ropensci/targets), an
    [`renv`](https://rstudio.github.io/renv/articles/renv.html)
    [snapshot](../renv.lock) is provided within this repository).

-   Knit the provided [targets
    markdown](https://books.ropensci.org/targets/markdown.html) file to
    setup the `targets` pipeline.

    ``` r
    knitr::knit("figure_targets.Rmd")
    ```

-   Download the required data files from
    [figshare](https://figshare.com/s/d02737c511ec6e74431a)
    ([`data-figures.zip`](https://figshare.com/s/d02737c511ec6e74431a))
    and extract the archive in the folder of this repository.

-   Execute the `targets` pipeline

    ``` r
    targets::tar_make()
    ```

-   Check the rendered figures within the `figures` folder.
